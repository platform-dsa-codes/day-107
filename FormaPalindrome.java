//{ Driver Code Starts
//Initial Template for Java

import java.io.*;
import java.util.*;

class GFG
{
    public static void main(String args[])throws IOException
    {
        BufferedReader read = new BufferedReader(new InputStreamReader(System.in));
        int t = Integer.parseInt(read.readLine());
        while(t-- > 0)
        {
            
            String S = read.readLine().trim();
            Solution ob = new Solution();
            System.out.println(ob.findMinInsertions(S));
        }
    }
}
// } Driver Code Ends


class Solution {
    int findMinInsertions(String S) {
        int n = S.length();
        
        // Create a 2D array to store the minimum number of insertions required
        int[][] dp = new int[n][n];
        
        // Fill the dp array using bottom-up dynamic programming approach
        for (int l = 2; l <= n; l++) {
            for (int i = 0; i < n - l + 1; i++) {
                int j = i + l - 1;
                
                // If the substring from i to j is already a palindrome
                if (S.charAt(i) == S.charAt(j) && l == 2) {
                    dp[i][j] = 0;
                } else if (S.charAt(i) == S.charAt(j)) {
                    dp[i][j] = dp[i + 1][j - 1];
                } else {
                    dp[i][j] = Math.min(dp[i + 1][j], dp[i][j - 1]) + 1;
                }
            }
        }
        
        // Return the minimum number of insertions required for the entire string
        return dp[0][n - 1];
    }
}
