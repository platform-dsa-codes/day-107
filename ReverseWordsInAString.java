class Solution {
    public String reverseWords(String s) {
        // Split the input string into words
        String[] words = s.trim().split("\\s+");

        // Create a StringBuilder to store the reversed string
        StringBuilder reversed = new StringBuilder();

        // Iterate through the words array in reverse order
        for (int i = words.length - 1; i >= 0; i--) {
            // Append each word to the StringBuilder
            reversed.append(words[i]);
            // If it's not the first word, append a space
            if (i > 0) {
                reversed.append(" ");
            }
        }

        // Convert the StringBuilder to a string and return
        return reversed.toString();
    }
}
